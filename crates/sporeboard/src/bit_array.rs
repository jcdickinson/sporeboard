use core::{marker::PhantomData, mem::size_of, ops, slice::Iter};

pub trait BitArrayExt {
    const ELEMENT_BIT_COUNT: usize;
    type Element;
    fn set_bit(&mut self, index: usize, value: bool) -> Option<bool>;
    fn get_bit(&self, index: usize) -> Option<bool>;
    fn bit_len(&self) -> usize;
    fn bit_iter<'a>(&'a self) -> BitIterator<'a, Self::Element>;
    fn set_bits(&mut self, i: impl Iterator<Item = bool>) -> usize;
}

macro_rules! zero {
    () => {
        unsafe { TryInto::<T>::try_into(0usize).unwrap_unchecked() }
    };
}

macro_rules! one {
    () => {
        unsafe { TryInto::<T>::try_into(1usize).unwrap_unchecked() }
    };
}

macro_rules! bit_array_index_and_mask {
    ($index: expr) => {{
        let size = size_of::<T>() * 8;
        let byte = $index / size;
        let bit: i32 = ($index % size) as i32;
        let mask = one!();
        let mask = mask << bit;

        (byte, mask)
    }};
}

impl<T> BitArrayExt for [T]
where
    T: Copy
        + PartialEq
        + TryFrom<usize>
        + ops::Not<Output = T>
        + ops::Div<Output = T>
        + ops::Shl<i32, Output = T>
        + ops::BitAnd<T, Output = T>
        + ops::BitOr<T, Output = T>,
{
    const ELEMENT_BIT_COUNT: usize = size_of::<T>() * 8;
    type Element = T;

    #[inline]
    fn set_bit(&mut self, index: usize, value: bool) -> Option<bool> {
        if index >= self.len() * Self::ELEMENT_BIT_COUNT {
            return None;
        }
        let (index, mask) = bit_array_index_and_mask!(index);
        let byte = &mut self[index];
        let result = *byte & mask == mask;
        if value {
            *byte = *byte | mask;
        } else {
            *byte = *byte & !mask;
        }

        Some(result)
    }

    #[inline]
    fn get_bit(&self, index: usize) -> Option<bool> {
        if index >= self.len() * Self::ELEMENT_BIT_COUNT {
            return None;
        }
        let (index, mask) = bit_array_index_and_mask!(index);
        let byte = self[index];
        Some(byte & mask == mask)
    }

    #[inline]
    fn bit_len(&self) -> usize {
        self.len() * Self::ELEMENT_BIT_COUNT
    }

    #[inline]
    fn bit_iter<'a>(&'a self) -> BitIterator<'a, T> {
        BitIterator {
            inner: self.iter(),
            current: zero!(),
            bit: i32::MAX,
            a: PhantomData::default(),
        }
    }

    #[inline]
    fn set_bits(&mut self, i: impl Iterator<Item = bool>) -> usize {
        if self.len() == 0 {
            return 0;
        }

        let mut mask = one!();
        let mut buf = self;
        let mut set = 0;

        for value in i {
            if mask == zero!() {
                buf = &mut buf[1..];
                if buf.len() == 0 {
                    break;
                }
                mask = one!();
            }
            set += 1;

            let val = &mut buf[0];
            if value {
                *val = *val | mask;
            } else {
                *val = *val & !mask;
            }

            mask = mask << 1;
        }

        set
    }
}

impl<T, const N: usize> BitArrayExt for [T; N]
where
    T: Copy
        + PartialEq
        + TryFrom<usize>
        + ops::Not<Output = T>
        + ops::Div<Output = T>
        + ops::Shl<i32, Output = T>
        + ops::BitAnd<T, Output = T>
        + ops::BitOr<T, Output = T>,
{
    const ELEMENT_BIT_COUNT: usize = size_of::<T>() * 8;
    type Element = T;

    #[inline]
    fn set_bit(&mut self, index: usize, value: bool) -> Option<bool> {
        self[..].set_bit(index, value)
    }

    #[inline]
    fn get_bit(&self, index: usize) -> Option<bool> {
        self[..].get_bit(index)
    }

    #[inline]
    fn bit_len(&self) -> usize {
        self[..].bit_len()
    }

    #[inline]
    fn bit_iter<'a>(&'a self) -> BitIterator<'a, T> {
        self[..].bit_iter()
    }

    #[inline]
    fn set_bits(&mut self, i: impl Iterator<Item = bool>) -> usize {
        self[..].set_bits(i)
    }
}

pub const fn bits_to_elements<T>(length: usize) -> usize {
    let size = size_of::<T>() * 8;
    (length + size - 1) / size
}

pub struct BitIterator<'a, T: 'a> {
    inner: Iter<'a, T>,
    current: T,
    bit: i32,
    a: PhantomData<&'a T>,
}

impl<'a, T: 'a> Iterator for BitIterator<'a, T>
where
    T: Copy + PartialEq + TryFrom<usize> + ops::Shl<i32, Output = T> + ops::BitAnd<T, Output = T>,
{
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        if self.bit >= (size_of::<T>() * 8) as i32 {
            if let Some(current) = self.inner.next() {
                self.current = *current;
                self.bit = 0;
            } else {
                self.current = zero!();
                return None;
            }
        }

        let mask = one!() << self.bit;
        self.bit += 1;
        Some(self.current & mask == mask)
    }
}

#[cfg(test)]
mod test {
    use assert2::check;

    use super::*;

    #[test]
    fn set_get_u8() {
        let mut sut = [0u8; 13];

        for i in 0..sut.bit_len() {
            check!(sut.set_bit(i, true) == Some(false));
            check!(sut.get_bit(i) == Some(true));
            check!(sut.set_bit(i, false) == Some(true));
            check!(sut.set_bit(i, i % 3 == 0) == Some(false));
        }

        check!(
            sut == [0x49, 0x92, 0x24, 0x49, 0x92, 0x24, 0x49, 0x92, 0x24, 0x49, 0x92, 0x24, 0x49]
        );
    }

    #[test]
    fn set_get_u16() {
        let mut sut = [0u16; 13];
        for i in 0..sut.bit_len() {
            check!(sut.set_bit(i, true) == Some(false));
            check!(sut.get_bit(i) == Some(true));
            check!(sut.set_bit(i, false) == Some(true));
            check!(sut.set_bit(i, i % 3 == 0) == Some(false));
        }
    }

    #[test]
    fn set_get_u32() {
        let mut sut = [0u32; 13];

        for i in 0..sut.bit_len() {
            check!(sut.set_bit(i, true) == Some(false));
            check!(sut.get_bit(i) == Some(true));
            check!(sut.set_bit(i, false) == Some(true));
            check!(sut.set_bit(i, i % 3 == 0) == Some(false));
        }
    }

    #[test]
    fn set_get_u64() {
        let mut sut = [0u64; 13];

        for i in 0..sut.bit_len() {
            check!(sut.set_bit(i, true) == Some(false));
            check!(sut.get_bit(i) == Some(true));
            check!(sut.set_bit(i, false) == Some(true));
            check!(sut.set_bit(i, i % 3 == 0) == Some(false));
        }
    }

    #[test]
    fn get_iter_u64() {
        let mut sut = [0u64; 13];

        for i in 0..sut.bit_len() {
            check!(sut.set_bit(i, i % 3 == 0 || i % 5 == 0) == Some(false));
        }

        for (i, k) in sut.bit_iter().enumerate() {
            check!(k == (i % 3 == 0 || i % 5 == 0));
        }
    }

    #[test]
    fn set_iter_u64() {
        let mut sut = [0u64; 13];

        let num_set = sut.set_bits(
            (0..sut.bit_len() * 2)
                .into_iter()
                .map(|i| i % 3 == 0 || i % 5 == 0),
        );
        check!(num_set == sut.bit_len());

        for (i, k) in sut.bit_iter().enumerate() {
            check!(k == (i % 3 == 0 || i % 5 == 0));
        }
    }
}
