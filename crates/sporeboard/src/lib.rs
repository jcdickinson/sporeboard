#![cfg_attr(target_os = "none", no_std)]

pub mod bit_array;
pub mod keyboard;

pub(crate) mod private {
    pub trait Sealed {}
}
