use core::cmp::Ordering;
use usbd_hid::descriptor::generator_prelude::*;

use crate::bit_array::BitArrayExt;

use super::KeyCode;

/// USB VIP for a generic keyboard from
/// https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt
pub const GENERIC_VID: u16 = 0x16c0;

/// USB PID for a generic keyboard from
/// https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt
pub const GENERIC_PID: u16 = 0x27db;

/// NKRO and BOOT compatible keyboard report and descriptor
///
/// This is achieved by declaring the boot report data as padding, causing fully compliant host
/// drivers to ignore that data. Firmware menus, which typically ignore the descriptor, will
/// interpret the padding as data.
///
/// See: https://www.devever.net/~hl/usbnkro
#[gen_hid_descriptor(
    (collection = APPLICATION, usage_page = GENERIC_DESKTOP, usage = KEYBOARD) = {
        (usage_min = 0xE0, usage_max = 0xE7) = {
            #[item_settings constant] modifier=input;
        };
        (usage_min = 0x00, usage_max = 0xFF) = {
            #[item_settings constant] reserved=input;
        };
        (usage_page = LEDS, usage_min = 0x01, usage_max = 0x05) = {
            #[packed_bits 5] #[item_settings data,variable,absolute] leds=output;
        };
        (usage_min = 0x00, usage_max = 0xDD) = {
            #[item_settings constant] keycodes=input;
        };
        (usage_page = KEYBOARD, usage_min = 0x04, usage_max = 0xA4) = {
            #[packed_bits 160] #[item_settings data,variable,absolute] keybitmap_low=input;
        };
        (usage_page = KEYBOARD, usage_min = 0xE0, usage_max = 0xFB) = {
            #[packed_bits 27] #[item_settings data,variable,absolute] keybitmap_high=input;
        };
    }
)]
#[allow(dead_code)]
#[derive(Default)]
pub struct BitmapKeyboardReport {
    pub modifier: u8,
    pub reserved: u8,
    pub leds: u8,
    pub keycodes: [u8; 6],
    pub keybitmap_low: [u8; 20],
    pub keybitmap_high: [u8; 4],
}

static ROLLOVER_ERRORS: [u8; 6] = [KeyCode::ErrorRollOver as u8; 6];

impl BitmapKeyboardReport {
    fn set_key_bit(&mut self, key: u8, value: bool) -> Option<bool> {
        let key = key as usize;
        if key <= 0xA4 {
            self.keybitmap_low.set_bit(key - 0x04, value)
        } else {
            self.keybitmap_high.set_bit(key - 0xE0, value)
        }
    }

    fn get_key_bit(&self, key: u8) -> Option<bool> {
        let key = key as usize;
        if key <= 0xA4 {
            self.keybitmap_low.get_bit(key - 0x04)
        } else {
            self.keybitmap_high.get_bit(key - 0xE0)
        }
    }

    /// Gets the state of a key
    pub fn get_key(&self, key: KeyCode) -> bool {
        if key < KeyCode::A {
            return false;
        }

        self.get_key_bit(key as u8).unwrap()
    }

    /// Sets a key in the bitmap to a specific state
    ///
    /// This will not update the 6KRO compatability. `update_6kro` must be called if this function
    /// returns `true`.
    ///
    /// # Returns
    ///
    /// Whether the bitmap was changed
    pub fn set_bitmap_key(&mut self, key: KeyCode, press: bool) -> bool {
        if key < KeyCode::A {
            return false;
        }

        if let Some(existing) = self.set_key_bit(key as u8, press) {
            existing != press
        } else {
            false
        }
    }

    /// Sets a key to a specific state, and updates the 6KRO compatability
    ///
    /// # Returns
    ///
    /// Whether an update actually occurred
    pub fn set_key(&mut self, key: KeyCode, press: bool) -> bool {
        if self.set_bitmap_key(key, press) {
            self.update_6kro();
            true
        } else {
            false
        }
    }

    /// Updates the 6KRO compatability
    pub fn update_6kro(&mut self) {
        let mut i = 0;

        // Doing the modifiers first allows us to return early when the 6-key limit is reached
        // because modifier keys will never roll-over
        for (index, byte) in self.keybitmap_high.iter().enumerate().filter(|x| *x.1 != 0) {
            let mut kc = 0xE0 + (index as u8 * 8) - 1;
            let mut byte = *byte;
            while byte != 0 {
                let set = byte & 1 == 1;
                kc += 1;
                byte = byte.overflowing_shr(1).0;
                if !set {
                    continue;
                }

                match (KeyCode::as_modifier_bit_raw(kc), i.cmp(&6)) {
                    (Some(modifier), _) => self.modifier |= modifier,
                    (None, Ordering::Less) => {
                        self.keycodes[i] = kc;
                        i += 1
                    }
                    (None, Ordering::Equal) => {
                        self.keycodes.copy_from_slice(ROLLOVER_ERRORS.as_ref());
                        return;
                    }
                    _ => {}
                }
            }
        }

        for (index, byte) in self.keybitmap_low.iter().enumerate().filter(|x| *x.1 != 0) {
            let mut kc = 0x04 + (index as u8 * 8) - 1;
            let mut byte = *byte;
            while byte != 0 {
                let set = byte & 1 == 1;
                kc += 1;
                byte = byte.overflowing_shr(1).0;
                if !set {
                    continue;
                }

                match i.cmp(&6) {
                    Ordering::Less => {
                        self.keycodes[i] = kc;
                        i += 1;
                    }
                    _ => {
                        self.keycodes.copy_from_slice(ROLLOVER_ERRORS.as_ref());
                        return;
                    }
                }
            }
        }

        for i in i..6 {
            self.keycodes[i] = 0;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use assert2::check;

    #[test]
    pub fn add_keys() {
        use KeyCode::*;

        let mut sut = BitmapKeyboardReport::default();

        fn get_test_keys(sut: &BitmapKeyboardReport) -> [bool; 7] {
            [
                sut.get_key(A),
                sut.get_key(B),
                sut.get_key(C),
                sut.get_key(D),
                sut.get_key(E),
                sut.get_key(Z),
                sut.get_key(MediaMute),
            ]
        }

        fn get_key_codes(sut: &BitmapKeyboardReport) -> [KeyCode; 6] {
            [
                sut.keycodes[0].into(),
                sut.keycodes[1].into(),
                sut.keycodes[2].into(),
                sut.keycodes[3].into(),
                sut.keycodes[4].into(),
                sut.keycodes[5].into(),
            ]
        }

        sut.set_key(A, true);
        check!(get_test_keys(&sut) == [true, false, false, false, false, false, false]);
        check!(get_key_codes(&sut) == [A, No, No, No, No, No]);

        sut.set_key(B, true);
        check!(get_test_keys(&sut) == [true, true, false, false, false, false, false]);
        check!(get_key_codes(&sut) == [A, B, No, No, No, No]);

        sut.set_key(B, true);
        sut.set_key(C, true);
        check!(get_test_keys(&sut) == [true, true, true, false, false, false, false]);
        check!(get_key_codes(&sut) == [A, B, C, No, No, No]);

        sut.set_key(MediaMute, true);
        check!(get_test_keys(&sut) == [true, true, true, false, false, false, true]);
        check!(get_key_codes(&sut) == [MediaMute, A, B, C, No, No]);

        sut.set_key(Z, true);
        check!(get_test_keys(&sut) == [true, true, true, false, false, true, true]);
        check!(get_key_codes(&sut) == [MediaMute, A, B, C, Z, No]);

        sut.set_key(D, true);
        check!(get_test_keys(&sut) == [true, true, true, true, false, true, true]);
        check!(get_key_codes(&sut) == [MediaMute, A, B, C, D, Z,]);

        sut.set_key(E, true);
        check!(get_test_keys(&sut) == [true, true, true, true, true, true, true]);
        check!(
            get_key_codes(&sut)
                == [
                    ErrorRollOver,
                    ErrorRollOver,
                    ErrorRollOver,
                    ErrorRollOver,
                    ErrorRollOver,
                    ErrorRollOver,
                ]
        );

        sut.set_key(MediaMute, false);
        sut.set_key(C, false);
        check!(get_test_keys(&sut) == [true, true, false, true, true, true, false]);
        check!(get_key_codes(&sut) == [A, B, D, E, Z, No]);

        sut.set_key(LCtrl, true);
        check!(get_test_keys(&sut) == [true, true, false, true, true, true, false]);
        check!(get_key_codes(&sut) == [A, B, D, E, Z, No]);
        check!(sut.modifier == 0x1);

        sut.set_key(LShift, true);
        check!(get_test_keys(&sut) == [true, true, false, true, true, true, false]);
        check!(get_key_codes(&sut) == [A, B, D, E, Z, No]);
        check!(sut.modifier == 0x3);
    }
}
