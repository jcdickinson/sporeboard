use embassy_time::{Duration, Instant};

enum ValueState<T> {
    None(T),
    Pending(T, Instant),
}

impl<T> ValueState<T> {
    fn get(&self) -> &T {
        match &self {
            Self::None(value) => value,
            Self::Pending(value, _) => value,
        }
    }
}

pub struct Debouncer<T: Clone> {
    value: ValueState<T>,
    wait: Duration,
}

impl<T: PartialEq + Clone> Debouncer<T> {
    pub fn new(debounce_time: Duration, initial_value: T) -> Self {
        Self {
            wait: debounce_time,
            value: ValueState::None(initial_value),
        }
    }

    pub fn next_timeout(&self) -> Option<Instant> {
        match self.value {
            ValueState::Pending(_, timeout) => Some(timeout),
            _ => None,
        }
    }

    pub fn set(&mut self, value: T) {
        if self.value.get() != &value {
            self.value = ValueState::Pending(value, Instant::now() + self.wait);
        }
    }

    pub fn next(&mut self) -> Option<T> {
        match &self.value {
            ValueState::Pending(value, time) if *time <= Instant::now() => {
                let new = ValueState::None(value.clone());
                if let ValueState::Pending(value, _) = core::mem::replace(&mut self.value, new) {
                    Some(value)
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}
