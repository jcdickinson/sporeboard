#![cfg_attr(target_os = "none", no_std)]
#![no_main]
#![feature(type_alias_impl_trait)]

mod ws2812;

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::{
    bind_interrupts,
    clocks::ClockConfig,
    gpio::{AnyPin, Input, Pull},
    i2c::InterruptHandler as I2cInterruptHandler,
    i2c_slave::{Command, I2cSlave, ReadStatus},
    peripherals::{I2C1, PIO0},
    pio::{InterruptHandler as PioInterruptHandler, Pio},
};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal::Signal};
use embassy_time::{Duration, Timer};
use smart_leds::RGB8;
use sporeboard::{bit_array::BitArrayExt, keyboard::Debouncer};
use ws2812::Ws2812;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
    PIO0_IRQ_0 => PioInterruptHandler<PIO0>;
    I2C1_IRQ => I2cInterruptHandler<I2C1>;
});

fn wheel(mut wheel_pos: u8) -> RGB8 {
    wheel_pos = 255 - wheel_pos;
    if wheel_pos < 85 {
        return (255 - wheel_pos * 3, 0, wheel_pos * 3).into();
    }
    if wheel_pos < 170 {
        wheel_pos -= 85;
        return (0, wheel_pos * 3, 255 - wheel_pos * 3).into();
    }
    wheel_pos -= 170;
    (wheel_pos * 3, 255 - wheel_pos * 3, 0).into()
}

static INPUT_READY: Signal<CriticalSectionRawMutex, [u8; 3]> = Signal::new();
static ENABLE_BACKLIGHT: Signal<CriticalSectionRawMutex, bool> = Signal::new();

#[cfg(feature = "left")]
static I2C_ADDRESS: u16 = 20;
#[cfg(feature = "right")]
static I2C_ADDRESS: u16 = 21;

#[embassy_executor::task]
async fn input(pins: [AnyPin; 18]) {
    let pins: heapless::Vec<_, 18> = pins
        .into_iter()
        .map(|pin| {
            let mut result = Input::new(pin, Pull::Up);
            result.set_schmitt(true);
            result
        })
        .collect();
    let pins = pins
        .into_array::<18>()
        .unwrap_or_else(|_| defmt::unreachable!());
    let mut debounce = Debouncer::new(Duration::from_millis(3), [0u8; 3]);

    loop {
        Timer::after(Duration::from_millis(1)).await;

        if let Some(next) = debounce.next() {
            info!("keys: {}", next);
            INPUT_READY.signal(next);
        }

        let mut next_state = [0u8; 3];
        next_state.set_bits(pins.iter().map(|pin| pin.is_low()));
        debounce.set(next_state);
    }
}

#[embassy_executor::task]
async fn rgb(mut ws2812: Ws2812<'static, PIO0, 0, 18>) {
    const NUM_LEDS: usize = 18;
    let mut data = [RGB8::default(); NUM_LEDS];
    let mut enable = false;

    loop {
        if ENABLE_BACKLIGHT.signaled() {
            enable = ENABLE_BACKLIGHT.wait().await;
        }

        if enable {
            for j in 0..(256 * 5) {
                for i in 0..NUM_LEDS {
                    data[i] =
                        wheel((((i * 256) as u16 / NUM_LEDS as u16 + j as u16) & 255) as u8) / 32;
                }
                ws2812.write(&data).await;

                Timer::after(Duration::from_millis(10)).await;
            }
        } else {
            for j in 0..(18 * 5) {
                let index = j / 5;
                data[index] = RGB8::new(16, 16, 16);
                ws2812.write(&data).await;
                Timer::after(Duration::from_millis(10)).await;
                data[index] = RGB8::new(0, 0, 0);
            }
        }
    }
}

#[embassy_executor::task]
async fn i2c_task(mut dev: I2cSlave<'static, I2C1>) -> ! {
    let mut keyboard_state = [0u8; 3];
    let mut buf = [0u8; 128];
    //let mut register = None;

    loop {
        if INPUT_READY.signaled() {
            keyboard_state = INPUT_READY.wait().await;
        }

        match dev.listen(&mut buf).await {
            Ok(Command::GeneralCall(len)) => {
                debug!("unsupported general call");
            }
            Ok(Command::Read) => {
                debug!("unknown read");
                unwrap!(dev.respond_till_stop(0).await);
            }
            Ok(Command::Write(len)) => match buf[0] {
                0x1 => {
                    debug!("write to backlight: {:?}", &buf[..len]);
                    ENABLE_BACKLIGHT.signal(buf[1] != 0);
                }
                _ => {
                    error!("unknown write: {:?}", buf[..len]);
                }
            },
            Ok(Command::WriteRead(len)) => match buf[0] {
                0x0 => {
                    debug!("read from keyboard state: {:?}", &buf[..len]);
                    unwrap!(dev.respond_to_read(&keyboard_state).await);
                }
                _ => {
                    trace!("unknown writeread {}", &buf[..len]);
                    unwrap!(dev.respond_till_stop(0).await);
                }
            },
            Err(e) => error!("err: {:?}", e),
        }
    }
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let mut config = embassy_rp::config::Config::default();
    config.clocks = ClockConfig::crystal(12_000_000);

    let p = embassy_rp::init(config);
    let mut pio = Pio::new(p.PIO0, Irqs);

    spawner.must_spawn(input([
        p.PIN_4.into(),
        p.PIN_3.into(),
        p.PIN_2.into(),
        p.PIN_1.into(),
        p.PIN_0.into(),
        p.PIN_9.into(),
        p.PIN_8.into(),
        p.PIN_7.into(),
        p.PIN_6.into(),
        p.PIN_5.into(),
        p.PIN_14.into(),
        p.PIN_13.into(),
        p.PIN_12.into(),
        p.PIN_11.into(),
        p.PIN_10.into(),
        p.PIN_18.into(),
        p.PIN_17.into(),
        p.PIN_16.into(),
    ]));

    let ws2812 = ws2812::Ws2812::new(&mut pio.common, pio.sm0, p.DMA_CH0, p.PIN_19);
    spawner.must_spawn(rgb(ws2812));

    info!("address: {:?}", I2C_ADDRESS);

    let mut config = embassy_rp::i2c_slave::Config::default();
    config.addr = I2C_ADDRESS;
    let device = I2cSlave::new(p.I2C1, p.PIN_27, p.PIN_26, Irqs, config);
    spawner.must_spawn(i2c_task(device));
}
