#![cfg_attr(target_os = "none", no_std)]
#![no_main]
#![feature(type_alias_impl_trait)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_futures::{
    join::join,
    select::{select, select3, Either, Either3},
};
use embassy_rp::{
    bind_interrupts,
    clocks::ClockConfig,
    gpio::{AnyPin, Input, Pull},
    i2c::{self, AbortReason, Error, InterruptHandler as I2cInterruptHandler},
    peripherals::{I2C1, USB},
    uart,
    usb::{Driver, InterruptHandler as UsbInterruptHandler},
};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal::Signal};
use embassy_time::{Duration, Instant, Timer};
use embassy_usb::{
    class::hid::{HidReaderWriter, ReportId, RequestHandler, State},
    control::OutResponse,
    Builder, Config, Handler,
};
use embedded_hal_async::i2c::{I2c, Operation};
use sporeboard::{
    bit_array::BitArrayExt,
    keyboard::{BitmapKeyboardReport, KeyCode},
};
use usbd_hid::descriptor::SerializedDescriptor;
use {defmt_rtt as _, panic_probe as _};

use core::sync::atomic::{AtomicBool, Ordering};

bind_interrupts!(struct Irqs {
    USBCTRL_IRQ => UsbInterruptHandler<USB>;
    I2C1_IRQ => I2cInterruptHandler<I2C1>;
});

static LEFT_SIGNAL: Signal<CriticalSectionRawMutex, [u8; 3]> = Signal::new();
static RIGHT_SIGNAL: Signal<CriticalSectionRawMutex, [u8; 3]> = Signal::new();
static POWERED_SIGNAL: Signal<CriticalSectionRawMutex, bool> = Signal::new();

static LEFT_LAYOUT: [KeyCode; 18] = [
    // Top
    KeyCode::W,
    KeyCode::L,
    KeyCode::Y,
    KeyCode::P,
    KeyCode::B,
    // Home
    KeyCode::C,
    KeyCode::R,
    KeyCode::S,
    KeyCode::T,
    KeyCode::G,
    // Bottom
    KeyCode::Q,
    KeyCode::J,
    KeyCode::V,
    KeyCode::D,
    KeyCode::K,
    // Thumb
    KeyCode::LBracket,
    KeyCode::Space,
    KeyCode::RBracket,
];

static RIGHT_LAYOUT: [KeyCode; 18] = [
    // Top
    KeyCode::SColon,
    KeyCode::U,
    KeyCode::O,
    KeyCode::F,
    KeyCode::Z,
    // Home
    KeyCode::A,
    KeyCode::I,
    KeyCode::E,
    KeyCode::N,
    KeyCode::M,
    // Bottom
    KeyCode::Dot,
    KeyCode::Comma,
    KeyCode::Slash,
    KeyCode::H,
    KeyCode::X,
    // Thumb
    KeyCode::LBracket,
    KeyCode::BSpace,
    KeyCode::RBracket,
];

static I2C_FREQUENCY: u32 = 10_000;

#[embassy_executor::task]
async fn usb_device(usb: USB) {
    let driver = Driver::new(usb, Irqs);

    let mut config = Config::new(
        sporeboard::keyboard::GENERIC_VID,
        sporeboard::keyboard::GENERIC_PID,
    );
    config.manufacturer = Some("sporeboard");
    config.product = Some("octoboard");
    config.serial_number = Some(env!("CARGO_PKG_VERSION"));
    config.max_power = 500;
    config.max_packet_size_0 = 64;

    let mut device_descriptor = [0; 256];
    let mut config_descriptor = [0; 256];
    let mut bos_descriptor = [0; 256];
    let mut control_buf = [0; 64];
    let request_handler = MyRequestHandler {};
    let mut device_handler = MyDeviceHandler::new();
    let mut state = State::new();

    let mut builder = Builder::new(
        driver,
        config,
        &mut device_descriptor,
        &mut config_descriptor,
        &mut bos_descriptor,
        &mut control_buf,
    );
    builder.handler(&mut device_handler);

    let config = embassy_usb::class::hid::Config {
        report_descriptor: sporeboard::keyboard::BitmapKeyboardReport::desc(),
        request_handler: Some(&request_handler),
        poll_ms: 10,
        max_packet_size: 64,
    };
    let hid = HidReaderWriter::<_, 1, 36>::new(&mut builder, &mut state, config);
    let mut usb = builder.build();
    let usb_fut = usb.run();

    let (reader, mut writer) = hid.split();

    let in_fut = async {
        let mut report = BitmapKeyboardReport::default();
        let mut next_checkin = Instant::MIN;

        loop {
            let now = Instant::now();
            let mut changed = false;
            match select3(
                LEFT_SIGNAL.wait(),
                RIGHT_SIGNAL.wait(),
                Timer::at(next_checkin),
            )
            .await
            {
                Either3::First(update) => {
                    for i in 0..18 {
                        changed |= report.set_key(LEFT_LAYOUT[i], update.get_bit(i).unwrap());
                    }
                }
                Either3::Second(update) => {
                    for i in 0..18 {
                        changed |= report.set_key(RIGHT_LAYOUT[i], update.get_bit(i).unwrap());
                    }
                }
                Either3::Third(_) => changed = true,
            }

            if changed || next_checkin <= Instant::now() {
                next_checkin = now + Duration::from_millis(5);
                match writer.write_serialize(&report).await {
                    Ok(()) => {}
                    Err(e) => warn!("Failed to send report {:?}", e),
                }
            }
        }
    };

    let out_fut = async {
        reader.run(false, &request_handler).await;
    };

    join(usb_fut, join(in_fut, out_fut)).await;
}

#[embassy_executor::task]
async fn controller_task(
    mut ready: Input<'static, AnyPin>,
    mut con: i2c::I2c<'static, I2C1, i2c::Async>,
) {
    let mut current_left = [0xFFu8; 3];
    let mut current_right = [0xFFu8; 3];
    let mut repeat = false;
    let mut powered = false;

    loop {
        if !ready.is_high() {
            info!("waiting for ready");
            LEFT_SIGNAL.signal([0u8; 3]);
            RIGHT_SIGNAL.signal([0u8; 3]);
            ready.wait_for_high().await;
            info!("waiting");
            Timer::after(Duration::from_micros(500)).await;
            info!("ready");
        }

        Timer::after(Duration::from_micros(500)).await;

        if POWERED_SIGNAL.signaled() {
            powered = POWERED_SIGNAL.wait().await;
            info!("Power state changed to {}", powered);
            repeat = true;
        }

        let buf = [0x1u8, if powered { 1 } else { 0 }];
        match con.write_async(20u16, buf).await {
            Ok(_) => trace!("Sent power state to L"),
            Err(Error::Abort(AbortReason::NoAcknowledge)) => (),
            Err(e) => {
                error!("(L) Error writing {:?}", e);
                repeat = true
            }
        }

        match con.write_async(21u16, buf).await {
            Ok(_) => trace!("Sent power state to R"),
            Err(Error::Abort(AbortReason::NoAcknowledge)) => (),
            Err(e) => {
                error!("(R) Error writing {:?}", e);
                repeat = true
            }
        }

        let mut resp_buff = [0u8; 3];
        match con.write_read(20u16, &[0], &mut resp_buff).await {
            Ok(_) => {
                debug!("(L) read: {} {:?}", Instant::now(), resp_buff);
                if resp_buff != current_left {
                    current_left = resp_buff;
                    LEFT_SIGNAL.signal(resp_buff)
                }
            }
            // Err(i2c::Error::Abort(AbortReason::NoAcknowledge)) => {
            //     if current_left != [0u8; 3] {
            //         current_left = [0u8; 3];
            //         RIGHT_SIGNAL.signal(current_left)
            //     }
            // }
            Err(Error::Abort(AbortReason::NoAcknowledge)) => (),
            Err(e) => error!("(L) Error writing {:?}", e),
        }

        let mut resp_buff = [0u8; 3];
        match con.write_read(21u16, &[0], &mut resp_buff).await {
            Ok(_) => {
                if resp_buff != current_right {
                    debug!("(R) read: {} {:?}", Instant::now(), resp_buff);
                    current_right = resp_buff;
                    RIGHT_SIGNAL.signal(resp_buff)
                }
            }
            // Err(i2c::Error::Abort(AbortReason::NoAcknowledge)) => {
            //     if current_right != [0u8; 3] {
            //         current_right = [0u8; 3];
            //         RIGHT_SIGNAL.signal(current_right)
            //     }
            // }
            Err(Error::Abort(AbortReason::NoAcknowledge)) => (),
            Err(e) => error!("(R) Error writing {:?}", e),
        }
        Timer::after(Duration::from_millis(1)).await;
    }
}

#[embassy_executor::task]
async fn logger_task(driver: Driver<'static, USB>) {
    embassy_usb_logger::run!(1024, log::LevelFilter::Debug, driver);
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let mut config = embassy_rp::config::Config::default();
    config.clocks = ClockConfig::crystal(12_000_000);
    let p = embassy_rp::init(config);

    //let driver = Driver::new(p.USB, Irqs);
    //spawner.spawn(logger_task(driver)).unwrap();
    Timer::after(Duration::from_secs(1)).await;
    spawner.must_spawn(usb_device(p.USB));

    let mut ready = Input::new(Into::<AnyPin>::into(p.PIN_28), Pull::Down);
    info!("waiting for ready");
    ready.wait_for_high().await;
    info!("ready");

    let mut config = i2c::Config::default();
    config.frequency = I2C_FREQUENCY;
    let controller = i2c::I2c::new_async(p.I2C1, p.PIN_27, p.PIN_26, Irqs, config);
    spawner.must_spawn(controller_task(ready, controller));
}

struct MyRequestHandler {}

impl RequestHandler for MyRequestHandler {
    fn get_report(&self, id: ReportId, _buf: &mut [u8]) -> Option<usize> {
        info!("Get report for {:?}", id);
        None
    }

    fn set_report(&self, id: ReportId, data: &[u8]) -> OutResponse {
        info!("Set report for {:?}: {:?}", id, data);
        OutResponse::Accepted
    }

    fn get_idle_ms(&self, id: Option<ReportId>) -> Option<u32> {
        info!("Get idle rate for {:?}", id);
        None
    }

    fn set_idle_ms(&self, id: Option<ReportId>, duration_ms: u32) {
        info!("Set idle rate for {:?} to {:?}", id, duration_ms);
    }
}

struct MyDeviceHandler {
    configured: AtomicBool,
}

impl MyDeviceHandler {
    fn new() -> Self {
        Self {
            configured: AtomicBool::new(false),
        }
    }
}

impl Handler for MyDeviceHandler {
    fn enabled(&mut self, enabled: bool) {
        self.configured.store(false, Ordering::Relaxed);
        POWERED_SIGNAL.signal(false);
        if enabled {
            info!("device enabled");
        } else {
            info!("device disabled");
        }
    }

    fn reset(&mut self) {
        self.configured.store(false, Ordering::Relaxed);
        POWERED_SIGNAL.signal(false);
        info!("Bus reset, the Vbus current limit is 100mA");
    }

    fn addressed(&mut self, addr: u8) {
        self.configured.store(false, Ordering::Relaxed);
        POWERED_SIGNAL.signal(false);
        info!("USB address set to: {}", addr);
    }

    fn configured(&mut self, configured: bool) {
        self.configured.store(configured, Ordering::Relaxed);
        POWERED_SIGNAL.signal(configured);
        if configured {
            info!("Device configured")
        } else {
            info!("Device unconfigured")
        }
    }
}
