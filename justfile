alias l := left
alias r := right
alias c := center

left: (run "left")
right: (run "right")
center: (run "center")

run side:
  #!/usr/bin/env bash
  set -euo pipefail

  ARGS=( )
  case "{{side}}" in
    left | right )
      cd "crates/boards/octoboard-kb"
      ARGS=( --no-default-features --features "{{side}}" )
      ;;
    center )
      cd "crates/boards/octoboard-cont" 
      ARGS=( )
      ;;
  esac
  cargo run "${ARGS[@]}"
